package geometry;

public class Square {
    private int side;

    public Square(int side) {
        this.side = side;
    }

    public String toString() {
        return "Your square's sides are " + side + "cm big";
    }

    public int getSide() {
        return this.side;
    }

    public int getArea() {
        return this.side*this.side;
    }
}