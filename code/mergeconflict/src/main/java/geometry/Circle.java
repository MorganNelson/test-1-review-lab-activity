package geometry;

public class Circle {
    private double radius;
    public Circle(double radius){
        this.radius=radius;
    }
    public double getArea(){
        return Math.PI*Math.pow(2,radius);
    }
    public String toString(){
        return "The radius is: " +this.radius;
    }
}
