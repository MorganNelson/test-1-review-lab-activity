package geometry;

public class Shapes{
    public static void main(String[] args) {
        Square s1 = new Square(14);
        System.out.println(s1);
        System.out.println(s1.getArea() + " square cm is the area of your square");
        Circle circle = new Circle(3.5);
        System.out.println(circle);
        Triangle triangle = new Triangle(2,3);
        System.out.println(triangle);
        System.out.println(triangle.getArea());

    }
}